package com.joy.practice.server.handler;

import com.joy.common.Operation;
import com.joy.common.OperationResult;
import com.joy.common.RequestMessage;
import com.joy.common.ResponseMessage;
import com.joy.util.JsonUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OrderServerProcessHandler extends SimpleChannelInboundHandler<RequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, RequestMessage requestMessage) throws Exception {
        Operation operation = requestMessage.getMessageBody();
        OperationResult operationResult = operation.execute();

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setMessageHeader(requestMessage.getMessageHeader());
        responseMessage.setMessageBody(operationResult);
        log.info("========>response : {}", JsonUtil.toJson(responseMessage));

        channelHandlerContext.writeAndFlush(responseMessage);
    }
}
