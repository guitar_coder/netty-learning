package com.joy.practice.client.codec;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * 解决TCP中粘包和半包问题
 */
public class OrderFrameDecoder extends LengthFieldBasedFrameDecoder {
    public OrderFrameDecoder() {
        super(Integer.MAX_VALUE, 0, 2, 0, 2);
    }
}
