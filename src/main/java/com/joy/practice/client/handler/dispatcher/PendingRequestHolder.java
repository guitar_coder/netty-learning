package com.joy.practice.client.handler.dispatcher;

import com.joy.common.OperationResult;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PendingRequestHolder {

    private Map<Long, OperationResultFuture> map = new ConcurrentHashMap<Long, OperationResultFuture>();

    public void add(Long streamId,OperationResultFuture future){
        this.map.put(streamId, future);
    }

    public void set(Long streamId, OperationResult result){
        OperationResultFuture operationResultFuture = this.map.get(streamId);
        if (operationResultFuture != null) {
            operationResultFuture.setSuccess(result);
            this.map.remove(streamId);
        }
    }

}
