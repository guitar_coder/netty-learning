package com.joy.practice.client.handler.dispatcher;

import com.joy.common.OperationResult;
import io.netty.util.concurrent.DefaultPromise;

public class OperationResultFuture extends DefaultPromise<OperationResult> {
}
