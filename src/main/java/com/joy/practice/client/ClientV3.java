package com.joy.practice.client;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.joy.common.OperationResult;
import com.joy.common.RequestMessage;
import com.joy.common.order.OrderOperation;
import com.joy.practice.client.codec.OperationToMessageEncoder;
import com.joy.practice.client.codec.OrderFrameDecoder;
import com.joy.practice.client.codec.OrderFrameEncoder;
import com.joy.practice.client.codec.OrderProtocolDecoder;
import com.joy.practice.client.codec.OrderProtocolEncoder;
import com.joy.practice.client.handler.ResponseDispatcherHandler;
import com.joy.practice.client.handler.dispatcher.OperationResultFuture;
import com.joy.practice.client.handler.dispatcher.PendingRequestHolder;
import com.joy.util.IdUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * 使用{@link com.joy.practice.client.handler.ResponseDispatcherHandler}进行响应分发
 */
public class ClientV3 {

    public static void main(String[] args) throws InterruptedException {
        Bootstrap bootstrap = new Bootstrap();
        NioEventLoopGroup group = new NioEventLoopGroup();
        PendingRequestHolder requestHolder = new PendingRequestHolder();
        try {
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        protected void initChannel(NioSocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new OrderFrameDecoder())
                                .addLast(new OrderFrameEncoder())
                                .addLast(new OrderProtocolEncoder())
                                .addLast(new OrderProtocolDecoder())
                                .addLast(new ResponseDispatcherHandler(requestHolder))
                                .addLast(new OperationToMessageEncoder())
                                .addLast(new LoggingHandler(LogLevel.INFO));
                        }
                    });
            ChannelFuture future = bootstrap.connect("127.0.0.1", 8090).sync();
            long streamId = IdUtil.nextId();
            RequestMessage requestMessage = new RequestMessage(streamId, new OrderOperation(1002, "丝瓜"));
            OperationResultFuture resultFuture = new OperationResultFuture();
            requestHolder.add(streamId, resultFuture);
            future.channel().writeAndFlush(requestMessage);

            OperationResult operationResult = resultFuture.get();
            System.out.println(operationResult);
            future.channel().closeFuture().sync();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
            TimeUnit.MICROSECONDS.sleep(1000);
        }
    }

}
